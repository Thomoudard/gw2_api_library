import requests
import logging
from . import RESET, RED


class APICLient:
    _api_key: str
    _logger: logging.Logger

    def __init__(self, api_key: str = None) -> None:
        """
        Initialize APICLient object.

        Args:
            api_key (str, optional): default None: Your API key if required.
        """
        self._api_key = api_key
        self._logger = logging.getLogger(__name__)

    def _auth_header(self) -> dict:
        """
        Generates an authentication header using a bearer token.

        Returns:
            dict: The authentication header.
        """
        return {'Authorization': f'Bearer {self._api_key}'}

    def _requester(self, endpoint: str, auth: bool = True) -> dict:
        """
        Request an endpoint.

        Args:
            endpoint (str): The endpoint to requests.
            auth (bool, optional): Specify if the endpoint needs auth.

        Returns:
            dict: The json responded by API
        """
        try:
            response = requests.get(
                endpoint,
                headers=self._auth_header() if auth else {}
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError as http_error:
            self._logger.error(
                f"{RED}An HTTP error occurred: {http_error}.{RESET}")
            self._logger.error(
                f"{RED}Response content: {response.content}{RESET}")
            raise
        except BaseException as error:
            self._logger.error(f"{RED}An error occurred: {error}.{RESET}")
            raise
        return response.json()
