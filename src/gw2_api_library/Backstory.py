from . import API_BASE_URL
from .APIClient import APICLient


class Backstory:
    _ENDPOINT: str = f"{API_BASE_URL}/backstory"
    _ENDPOINT_ANSWERS: str = f"{_ENDPOINT}/answers"
    _ENDPOINT_QUESTIONS: str = f"{_ENDPOINT}/questions"

    _api_client: APICLient

    def __init__(self, api_client: APICLient) -> None:
        """
        Initialize Backstory object.

        Args:
            api_client (APIClient): The APIClient object. No API key required.
        """
        self._api_client = api_client

    def get_backstory(self) -> list:
        """
        Get backstory data.

        Returns:
            list: https://wiki.guildwars2.com/wiki/API:2/backstory
        """
        return self._api_client._requester(endpoint=self._ENDPOINT,
                                           auth=False)

    def get_answers(self, ids: str | list[str] = None) -> list | dict:
        """
        Get backstory answers data.

        Args:
            ids (list[str] | str, optional): List of answers IDs or a single answer ID.

        Returns:
            list | dict: A list of answers if `ids` is `None`, or a dictionary of answer(s) data if `ids` is specified. https://wiki.guildwars2.com/wiki/API:2/backstory/answers
        """
        if ids is None:
            endpoint = self._ENDPOINT_ANSWERS
        elif isinstance(ids, str):
            endpoint = f"{self._ENDPOINT_ANSWERS}/{ids}"
        elif isinstance(ids, list) and all(isinstance(id, str) for id in ids):
            endpoint = (
                f"{self._ENDPOINT_ANSWERS}"
                f"?ids={','.join(str(id) for id in ids)}"
            )
        else:
            raise ValueError("ids should be a list of str or a single str")

        return self._api_client._requester(endpoint=endpoint, auth=False)

    def get_questions(self, id: int = None) -> list | dict:
        """
        Get backstory questions data.

        Args:
            id (int, optional): A single answer ID.

        Returns:
            list | dict: A list of questions if `id` is `None`, or a dictionary of a question data if `id` is specified. https://wiki.guildwars2.com/wiki/API:2/backstory/questions
        """
        if id is None:
            endpoint = self._ENDPOINT_QUESTIONS
        elif isinstance(id, int):
            endpoint = f"{self._ENDPOINT_QUESTIONS}/{id}"
        else:
            raise ValueError("id should be a int")

        return self._api_client._requester(endpoint=endpoint, auth=False)
