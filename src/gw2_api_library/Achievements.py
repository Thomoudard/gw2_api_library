from . import API_BASE_URL
from .APIClient import APICLient


class Achievements:
    _ENDPOINT: str = f"{API_BASE_URL}/achievements"
    _ENDPOINT_CATEGORIES: str = f"{_ENDPOINT}/categories"
    _ENDPOINT_DAILY: str = f"{_ENDPOINT}/daily"
    _ENDPOINT_DAILY_TOMORROW: str = f"{_ENDPOINT_DAILY}/tomorrow"
    _ENDPOINT_GROUPS: str = f"{_ENDPOINT}/groups"

    _api_client: APICLient

    def __init__(self, api_client: APICLient) -> None:
        """
        Initialize Achievements object.

        Args:
            api_client (APIClient): The APIClient object. No API key required.
        """
        self._api_client = api_client

    def get_achievements(self, ids: int | list[int] = None) -> list | dict:
        """
        Get achievements data.

        Args:
            ids (list[int] | int, optional): List of achievement IDs or a single achievement ID.

        Returns:
            list | dict: A list of achievements if `ids` is `None`, or a dictionary of achievement(s) data if `ids` is specified. https://wiki.guildwars2.com/wiki/API:2/achievements
        """
        if ids is None:
            endpoint = self._ENDPOINT
        elif isinstance(ids, int):
            endpoint = f"{self._ENDPOINT}/{ids}"
        elif isinstance(ids, list) and all(isinstance(id, int) for id in ids):
            endpoint = (
                f"{self._ENDPOINT}"
                f"?ids={','.join(str(id) for id in ids)}"
            )
        else:
            raise ValueError("ids should be a list of int or a single int")

        return self._api_client._requester(endpoint=endpoint, auth=False)

    def get_categories(self, ids: int | list[int] = None) -> list | dict:
        """
        Get achievements categories data.

        Args:
            ids (list[int] | int, optional): List of categories IDs or a single category ID.

        Returns:
            list | dict: A list of categories if `ids` is `None`, or a dictionary of category(ies) data if `ids` is specified. https://wiki.guildwars2.com/wiki/API:2/achievements/categories
        """
        if ids is None:
            endpoint = self._ENDPOINT_CATEGORIES
        elif isinstance(ids, int):
            endpoint = f"{self._ENDPOINT_CATEGORIES}/{ids}"
        elif isinstance(ids, list) and all(isinstance(id, int) for id in ids):
            endpoint = (
                f"{self._ENDPOINT_CATEGORIES}"
                f"?ids={','.join(str(id) for id in ids)}"
            )
        else:
            raise ValueError("ids should be a int of IDs or a single int")

        return self._api_client._requester(endpoint=endpoint, auth=False)

    def get_daily(self):
        """
        API not active
        Get achievements daily data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/achievements/daily
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_DAILY,
                                           auth=False)

    def get_daily_tomorrow(self):
        """
        API not active
        Get achievements daily tomorrow data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/achievements/daily/tomorrow
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_DAILY_TOMORROW,
            auth=False)

    def get_groups(self, id: str = None) -> list | dict:
        """
        Get achievements groups data.

        Args:
            id (str, optional): A single group ID.

        Returns:
            list | dict: A list of groups if `id` is `None`, or a dictionary of a group data if `id` is specified. https://wiki.guildwars2.com/wiki/API:2/achievements/groups
        """
        if id is None:
            endpoint = self._ENDPOINT_GROUPS
        elif isinstance(id, str):
            endpoint = f"{self._ENDPOINT_GROUPS}/{id}"
        else:
            raise ValueError("id should be a str")

        return self._api_client._requester(endpoint=endpoint, auth=False)
