API_URL: str = "https://api.guildwars2.com"
API_VERSION: int = 2
API_BASE_URL: str = f"{API_URL}/v{API_VERSION}"

RESET: str = "\033[00m"
RED: str = "\033[91m"
