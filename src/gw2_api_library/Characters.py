from urllib.parse import quote
from . import API_BASE_URL
from .APIClient import APICLient


def _str_to_percent_encoded(name: str) -> str:
    return quote(f"{name}", safe='')


class Characters:
    _ENDPOINT: str = f"{API_BASE_URL}/characters"
    _ENDPOINT_ALL: str = f"{_ENDPOINT}?ids=all"
    _ENDPOINT

    _encoded_name: str
    _api_client: APICLient

    def __init__(self, api_client: APICLient) -> None:
        """
        Initialize Characters object.

        Args:
            api_client (APIClient): The APIClient object. No API key required.
        """
        self._api_client = api_client

    def set_characters(self, name: str):
        self._encoded_name = _str_to_percent_encoded(name=name)

    def get_characters(self):
        """
        Get characters data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/characters
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_ALL)

    def get_characters_name(self):
        """
        Get characters name data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/characters
        """
        return self._api_client._requester(endpoint=self._ENDPOINT)

    def get_characters_backstory(self):
        """
        Get characters backstory data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/characters/:id/backstory
        """
        return self._api_client._requester(
            endpoint=f"{self._ENDPOINT}/{self._encoded_name}/backstory")
