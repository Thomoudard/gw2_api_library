from . import API_BASE_URL
from .APIClient import APICLient


class Account:
    _ENDPOINT: str = f"{API_BASE_URL}/account"
    _ENDPOINT_ACHIEVEMENTS: str = f"{_ENDPOINT}/achievements"
    _ENDPOINT_BANK: str = f"{_ENDPOINT}/bank"
    _ENDPOINT_BUILD_STORAGE: str = f"{_ENDPOINT}/buildstorage"
    _ENDPOINT_DAILY_CRAFTING: str = f"{_ENDPOINT}/dailycrafting"
    _ENDPOINT_DUNGEONS: str = f"{_ENDPOINT}/dungeons"
    _ENDPOINT_DYES: str = f"{_ENDPOINT}/dyes"
    _ENDPOINT_EMOTES: str = f"{_ENDPOINT}/emotes"
    _ENDPOINT_FINISHERS: str = f"{_ENDPOINT}/finishers"
    _ENDPOINT_GLIDERS: str = f"{_ENDPOINT}/gliders"
    _ENDPOINT_HOME: str = f"{_ENDPOINT}/home"
    _ENDPOINT_HOME_CATS: str = f"{_ENDPOINT_HOME}/cats"
    _ENDPOINT_HOME_NODES: str = f"{_ENDPOINT_HOME}/nodes"
    _ENDPOINT_INVENTORY: str = f"{_ENDPOINT}/inventory"
    _ENDPOINT_LEGENDARY_ARMORY: str = f"{_ENDPOINT}/legendaryarmory"
    _ENDPOINT_LUCK: str = f"{_ENDPOINT}/luck"
    _ENDPOINT_MAIL_CARRIERS: str = f"{_ENDPOINT}/mailcarriers"
    _ENDPOINT_MAP_CHESTS: str = f"{_ENDPOINT}/mapchests"
    _ENDPOINT_MASTERIES: str = f"{_ENDPOINT}/masteries"
    _ENDPOINT_MASTERY_POINTS: str = f"{_ENDPOINT}/mastery/points"
    _ENDPOINT_MATERIALS: str = f"{_ENDPOINT}/materials"
    _ENDPOINT_MINIS: str = f"{_ENDPOINT}/minis"
    _ENDPOINT_MOUNTS: str = f"{_ENDPOINT}/mounts"
    _ENDPOINT_MOUNTS_SKINS: str = f"{_ENDPOINT_MOUNTS}/skins"
    _ENDPOINT_MOUNTS_TYPES: str = f"{_ENDPOINT_MOUNTS}/types"
    _ENDPOINT_NOVELTIES: str = f"{_ENDPOINT}/novelties"
    _ENDPOINT_OUTFITS: str = f"{_ENDPOINT}/outfits"
    _ENDPOINT_PVP_HEROES: str = f"{_ENDPOINT}/pvp/heroes"
    _ENDPOINT_RAIDS: str = f"{_ENDPOINT}/raids"
    _ENDPOINT_RECIPES: str = f"{_ENDPOINT}/recipes"
    _ENDPOINT_SKINS: str = f"{_ENDPOINT}/skins"
    _ENDPOINT_TITLES: str = f"{_ENDPOINT}/titles"
    _ENDPOINT_WALLET: str = f"{_ENDPOINT}/wallet"
    _ENDPOINT_WORLD_BOSSES: str = f"{_ENDPOINT}/worldbosses"

    _api_client: APICLient

    def __init__(self, api_client: APICLient) -> None:
        """
        Initialize Account object.

        Args:
            api_client (APIClient): The APIClient object with your API key.
        """
        self._api_client = api_client

    def get_account(self):
        """
        Get account data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account
        """
        return self._api_client._requester(endpoint=self._ENDPOINT)

    def get_achievements(self):
        """
        Get account achievements data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/achievements
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_ACHIEVEMENTS)

    def get_bank(self):
        """
        Get account bank data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/bank
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_BANK)

    def get_build_storage(self):
        """
        Get account build storage data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/buildstorage
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_BUILD_STORAGE)

    def get_daily_crafting(self):
        """
        Get account daily crafting data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/dailycrafting
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_DAILY_CRAFTING)

    def get_dungeons(self):
        """
        Get account dungeons data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/dungeons
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_DUNGEONS)

    def get_dyes(self):
        """
        Get account dyes data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/dyes
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_DYES)

    def get_emotes(self):
        """
        Get account emotes data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/emotes
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_EMOTES)

    def get_finishers(self):
        """
        Get account finishers data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/finishers
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_FINISHERS)

    def get_gliders(self):
        """
        Get account gliders data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/gliders
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_GLIDERS)

    def get_home(self) -> list:
        """
        Get account home data.

        Returns:
            list: https://wiki.guildwars2.com/wiki/API:2/account/home
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_HOME,
                                           auth=False)

    def get_home_cats(self):
        """
        Get account home cats data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/home/cats
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_HOME_CATS)

    def get_home_nodes(self):
        """
        Get account home nodes data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/home/nodes
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_HOME_NODES)

    def get_inventory(self):
        """
        Get account inventory data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/inventory
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_INVENTORY)

    def get_legendary_armory(self):
        """
        Get account legendary armory data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/legendaryarmory
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_LEGENDARY_ARMORY)

    def get_luck(self):
        """
        Get account luck data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/luck
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_LUCK)

    def get_mail_carriers(self):
        """
        Get account mail carriers data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/mailcarriers
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MAIL_CARRIERS)

    def get_map_chests(self):
        """
        Get account map chests data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/mapchests
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MAP_CHESTS)

    def get_masteries(self):
        """
        Get account masteries data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/masteries
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MASTERIES)

    def get_mastery_points(self):
        """
        Get account mastery points data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/mastery/points
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MASTERY_POINTS)

    def get_materials(self):
        """
        Get account materials data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/materials
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MATERIALS)

    def get_minis(self):
        """
        Get account minis data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/minis
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_MINIS)

    def get_mounts(self) -> list:
        """
        Get account mounts data.

        Returns:
            list: https://wiki.guildwars2.com/wiki/API:2/account/mounts
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_MOUNTS,
                                           auth=False)

    def get_mounts_skins(self):
        """
        Get account mounts skins data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/mounts/skins
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MOUNTS_SKINS)

    def get_mounts_types(self):
        """
        Get account mounts types data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/mounts/types
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_MOUNTS_TYPES)

    def get_novelties(self):
        """
        Get account novelties data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/novelties
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_NOVELTIES)

    def get_outfits(self):
        """
        Get account outfits data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/outfits
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_OUTFITS)

    def get_pvp_heroes(self):
        """
        Get account pvp heroes data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/pvp/heroes
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_PVP_HEROES)

    def get_raids(self):
        """
        Get account raids data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/raids
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_RAIDS)

    def get_recipes(self):
        """
        Get account recipes data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/recipes
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_RECIPES)

    def get_skins(self):
        """
        Get account skins data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/skins
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_SKINS)

    def get_titles(self):
        """
        Get account titles data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/titles
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_TITLES)

    def get_wallet(self):
        """
        Get account wallet data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/wallet
        """
        return self._api_client._requester(endpoint=self._ENDPOINT_WALLET)

    def get_world_bosses(self):
        """
        Get account world bosses data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/account/worldbosses
        """
        return self._api_client._requester(
            endpoint=self._ENDPOINT_WORLD_BOSSES)
