from . import API_BASE_URL
from .APIClient import APICLient


class Build:
    _ENDPOINT: str = f"{API_BASE_URL}/build"

    _api_client: APICLient

    def __init__(self, api_client: APICLient) -> None:
        """
        Initialize Build object.

        Args:
            api_client (APIClient): The APIClient object. No API key required.
        """
        self._api_client = api_client

    def get_build(self):
        """
        Get build data.

        Returns:
            dict: https://wiki.guildwars2.com/wiki/API:2/build
        """
        return self._api_client._requester(endpoint=self._ENDPOINT,
                                           auth=False)
