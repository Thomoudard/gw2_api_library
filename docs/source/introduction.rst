Introduction
============

This library is an open-source project to facilitate the use of the Guild Wars 2 api in a python project

Contributor
-----------

`Sfire <https://gitlab.com/Thomoudard>`_

Tools used
----------

`Poetry <https://python-poetry.org>`_
    - The following configuration is used ``poetry config virtualenvs.create true``

`Sphinx <https://www.sphinx-doc.org/en/master/index.html>`_
    - You can see the configuration under docs/source

`Docker-compose <https://docs.docker.com/compose>`_
    - Used to launch GitLab pipeline

`GitLab CI/CD <https://docs.gitlab.com/ee/ci>`_
    - Used to build this documentation and run some test