import os
from dotenv import load_dotenv
from gw2_api_library.APIClient import APICLient
from gw2_api_library.Account import Account

RESET: str = "\033[00m"
RED: str = "\033[91m"
GREEN: str = "\033[92m"

load_dotenv()

account = Account(APICLient(api_key=os.getenv("API_KEY")))
try:
    account.get_account()
    print(f"{GREEN}Test passed for the method get_account(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_account(){RESET}")
    pass

try:
    account.get_achievements()
    print(f"{GREEN}Test passed for the method get_achievements(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_achievements(){RESET}")
    pass

try:
    account.get_bank()
    print(f"{GREEN}Test passed for the method get_bank(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_bank(){RESET}")
    pass

try:
    account.get_build_storage()
    print(f"{GREEN}Test passed for the method get_build_storage(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_build_storage(){RESET}")
    pass

try:
    account.get_daily_crafting()
    print(f"{GREEN}Test passed for the method get_daily_crafting(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_daily_crafting(){RESET}")
    pass

try:
    account.get_dungeons()
    print(f"{GREEN}Test passed for the method get_dungeons(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_dungeons(){RESET}")
    pass

try:
    account.get_dyes()
    print(f"{GREEN}Test passed for the method get_dyes(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_dyes(){RESET}")
    pass

try:
    account.get_emotes()
    print(f"{GREEN}Test passed for the method get_emotes(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_emotes(){RESET}")
    pass

try:
    account.get_finishers()
    print(f"{GREEN}Test passed for the method get_finishers(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_finishers(){RESET}")
    pass

try:
    account.get_gliders()
    print(f"{GREEN}Test passed for the method get_gliders(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_gliders(){RESET}")
    pass

try:
    account.get_home()
    print(f"{GREEN}Test passed for the method get_home(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_home(){RESET}")
    pass

try:
    account.get_home_cats()
    print(f"{GREEN}Test passed for the method get_home_cats(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_home_cats(){RESET}")
    pass

try:
    account.get_home_nodes()
    print(f"{GREEN}Test passed for the method get_home_nodes(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_home_nodes(){RESET}")
    pass

try:
    account.get_inventory()
    print(f"{GREEN}Test passed for the method get_inventory(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_inventory(){RESET}")
    pass

try:
    account.get_legendary_armory()
    print(f"{GREEN}Test passed for the method get_legendary_armory(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_legendary_armory(){RESET}")
    pass

try:
    account.get_luck()
    print(f"{GREEN}Test passed for the method get_luck(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_luck(){RESET}")
    pass

try:
    account.get_mail_carriers()
    print(f"{GREEN}Test passed for the method get_mail_carriers(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_mail_carriers(){RESET}")
    pass

try:
    account.get_map_chests()
    print(f"{GREEN}Test passed for the method get_map_chests(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_map_chests(){RESET}")
    pass

try:
    account.get_masteries()
    print(f"{GREEN}Test passed for the method get_masteries(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_masteries(){RESET}")
    pass

try:
    account.get_mastery_points()
    print(f"{GREEN}Test passed for the method get_mastery_points(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_mastery_points(){RESET}")
    pass

try:
    account.get_materials()
    print(f"{GREEN}Test passed for the method get_materials(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_materials(){RESET}")
    pass

try:
    account.get_minis()
    print(f"{GREEN}Test passed for the method get_minis(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_minis(){RESET}")
    pass

try:
    account.get_mounts()
    print(f"{GREEN}Test passed for the method get_mounts(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_mounts(){RESET}")
    pass

try:
    account.get_mounts_skins()
    print(f"{GREEN}Test passed for the method get_mounts_skins(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_mounts_skins(){RESET}")
    pass

try:
    account.get_mounts_types()
    print(f"{GREEN}Test passed for the method get_mounts_types(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_mounts_types(){RESET}")
    pass

try:
    account.get_novelties()
    print(f"{GREEN}Test passed for the method get_novelties(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_novelties(){RESET}")
    pass

try:
    account.get_outfits()
    print(f"{GREEN}Test passed for the method get_outfits(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_outfits(){RESET}")
    pass

try:
    account.get_pvp_heroes()
    print(f"{GREEN}Test passed for the method get_pvp_heroes(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_pvp_heroes(){RESET}")
    pass

try:
    account.get_raids()
    print(f"{GREEN}Test passed for the method get_raids(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_raids(){RESET}")
    pass

try:
    account.get_recipes()
    print(f"{GREEN}Test passed for the method get_recipes(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_recipes(){RESET}")
    pass

try:
    account.get_skins()
    print(f"{GREEN}Test passed for the method get_skins(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_skins(){RESET}")
    pass

try:
    account.get_titles()
    print(f"{GREEN}Test passed for the method get_titles(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_titles(){RESET}")
    pass

try:
    account.get_wallet()
    print(f"{GREEN}Test passed for the method get_wallet(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_wallet(){RESET}")
    pass

try:
    account.get_world_bosses()
    print(f"{GREEN}Test passed for the method get_world_bosses(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_world_bosses(){RESET}")
    pass
