from gw2_api_library.APIClient import APICLient
from gw2_api_library.Backstory import Backstory

RESET: str = "\033[00m"
RED: str = "\033[91m"
GREEN: str = "\033[92m"

backstory = Backstory(APICLient())

try:
    backstory.get_backstory()
    print(f"{GREEN}Test passed for the method get_backstory(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_backstory(){RESET}")
    pass

try:
    lst_ans = backstory.get_answers()
    print(f"{GREEN}Test passed for the method get_answers(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_answers(){RESET}")
    pass

try:
    backstory.get_answers(ids=lst_ans[0])
    print(f"{GREEN}Test passed for the method get_answers(str){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_answers(str){RESET}")
    pass

try:
    backstory.get_answers(ids=[lst_ans[0], lst_ans[1]])
    print(f"{GREEN}Test passed for the method get_answers(list[str]){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_answers(list[str]){RESET}")
    pass

try:
    lst_qst = backstory.get_questions()
    print(f"{GREEN}Test passed for the method get_questions(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_questions(){RESET}")
    pass

try:
    backstory.get_questions(id=lst_qst[0])
    print(f"{GREEN}Test passed for the method get_questions(str){RESET}")
except BaseException as error:
    print(f"{RED}Test failed for the method get_questions(str){RESET}")
    print(error)
    pass
