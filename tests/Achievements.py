from gw2_api_library.APIClient import APICLient
from gw2_api_library.Achievements import Achievements

RESET: str = "\033[00m"
RED: str = "\033[91m"
GREEN: str = "\033[92m"

achievements = Achievements(APICLient())

try:
    lst_ach = achievements.get_achievements()
    print(f"{GREEN}Test passed for the method get_achievements(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_achievements(){RESET}")
    pass

try:
    achievements.get_achievements(ids=lst_ach[0])
    print(f"{GREEN}Test passed for the method get_achievements(int){RESET}")
except BaseException as error:
    print(f"{RED}Test failed for the method get_achievements(int){RESET}")
    print(error)
    pass

try:
    achievements.get_achievements(ids=[lst_ach[0], lst_ach[1]])
    print(
        f"{GREEN}Test passed for the method get_achievements(list[int]){RESET}"
    )
except BaseException:
    print(
        f"{RED}Test failed for the method get_achievements(list[int]){RESET}"
    )
    pass

try:
    lst_cat = achievements.get_categories()
    print(f"{GREEN}Test passed for the method get_categories(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_categories(){RESET}")
    pass

try:
    achievements.get_categories(ids=lst_cat[0])
    print(f"{GREEN}Test passed for the method get_categories(int){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_categories(int){RESET}")
    pass

try:
    achievements.get_categories(ids=[lst_cat[0], lst_cat[1]])
    print(
        f"{GREEN}Test passed for the method get_categories(list[int]){RESET}"
    )
except BaseException:
    print(f"{RED}Test failed for the method get_categories(list[int]){RESET}")
    pass

try:
    achievements.get_daily()
    print(f"{GREEN}Test passed for the method get_daily(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_daily(){RESET}")
    pass

try:
    achievements.get_daily_tomorrow()
    print(f"{GREEN}Test passed for the method get_daily_tomorrow(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_daily_tomorrow(){RESET}")
    pass

try:
    lst_grp = achievements.get_groups()
    print(f"{GREEN}Test passed for the method get_groups(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_groups(){RESET}")
    pass

try:
    achievements.get_groups(id=lst_grp[0])
    print(f"{GREEN}Test passed for the method get_groups(str){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_groups(str){RESET}")
    pass
