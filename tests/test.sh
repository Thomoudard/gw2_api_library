#!/bin/bash
for entry in `ls $(dirname "$0")/*.py`; do
    echo
    echo "--------------------"
    echo "${entry}"
    echo "--------------------"
    python3 $entry
done