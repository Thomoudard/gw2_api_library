from gw2_api_library.APIClient import APICLient
from gw2_api_library.Build import Build

RESET: str = "\033[00m"
RED: str = "\033[91m"
GREEN: str = "\033[92m"

build = Build(APICLient())

try:
    build.get_build()
    print(f"{GREEN}Test passed for the method get_build(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_build(){RESET}")
    pass
