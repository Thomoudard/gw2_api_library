import os
from dotenv import load_dotenv
from gw2_api_library.APIClient import APICLient
from gw2_api_library.Characters import Characters

RESET: str = "\033[00m"
RED: str = "\033[91m"
GREEN: str = "\033[92m"

load_dotenv()

characters = Characters(APICLient(api_key=os.getenv("API_KEY")))

try:
    characters.get_characters()
    print(f"{GREEN}Test passed for the method get_characters(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_characters(){RESET}")
    pass

try:
    lst_char = characters.get_characters_name()
    print(f"{GREEN}Test passed for the method get_characters_name(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_characters_name(){RESET}")
    pass

try:
    characters.set_characters(name=lst_char[0])
    print(f"{GREEN}Test passed for the method set_characters(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method set_characters(){RESET}")
    pass

try:
    characters.get_characters_backstory()
    print(
        f"{GREEN}Test passed for the method get_characters_backstory(){RESET}")
except BaseException:
    print(f"{RED}Test failed for the method get_characters_backstory(){RESET}")
    pass
